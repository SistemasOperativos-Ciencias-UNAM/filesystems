#!/bin/bash

NAME=prueba
test -d ${NAME} || mkdir -v ${NAME}
pushd ${NAME}

for MODE
in 000 400 440 444 600 640 644 660 664 666 700 701 711 750 751 771 775 777
do
  touch archivo-${MODE}
  chmod -c ${MODE} archivo-${MODE}
  ls -la archivo-${MODE}
  echo "Hola" >> archivo-${MODE}
  cat archivo-${MODE}
done
popd &>/dev/null
