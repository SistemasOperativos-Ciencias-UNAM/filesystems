#!/bin/bash

NAME=prueba_dir
test -d ${NAME} || mkdir -v ${NAME}
pushd ${NAME}

for DIR_MODE
in 000 400 440 444 600 640 644 660 664 666 500 501 510 511 700 701 711 750 751 771 775 777
do
  mkdir -v directorio-${DIR_MODE}
  chmod ${DIR_MODE} directorio-${DIR_MODE}
  pushd directorio-${DIR_MODE}
  # Si se pudo cambiar al directorio
  if [ "$?" -eq 0 ]
  then
    for MODE
    in 000 400 440 444 600 640 644 660 664 666 700 701 711 750 751 771 775 777
    do
      touch archivo-${MODE}
      chmod ${MODE} archivo-${MODE}
      ls -la archivo-${MODE}
      echo "Hola" >> archivo-${MODE}
      cat archivo-${MODE}
    done
    popd &>/dev/null
  fi
done
popd &>/dev/null
